﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataService.Models
{
    public enum ResourceType
    {
        Engineer,
        Cleaner,
        Electrician,
        Plumber
    }
}
