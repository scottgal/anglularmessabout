﻿using AutoMapper;

namespace TestDataService.Config
{
    public class AutoMapperConfig
    {
        public static void Setup()
        {
   
          Mapper.AssertConfigurationIsValid();
        }
    }
}