﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestApp.Models;
using TestDataService.Models;

namespace TestApp.Data
{
    public class AssetsContextInitializer : DropCreateDatabaseIfModelChanges<AssetsContext>
    {

        private Random rnd = new Random((int)DateTime.Now.Ticks);
        private Array _resourceTypeVals = Enum.GetValues(typeof(ResourceType));
        private List<Asset> GetChildAssets()
        {
            var recordCount = rnd.Next(100);

            for (int i = 0; i < recordCount; i++)
            {
                
            }

        }

        private Asset GetAsset()
        {
            return new Asset(){};
        }

        private Resource GetResource()
        {
            var resourceType = (ResourceType)rnd.Next(_resourceTypeVals.Length-1);
            var res = new Resource() {Name = Faker.Name.FullName(), RType = resourceType};
        
            return res;

        }

    

        private List<Asset> PopulateAssets()
        {
            
        }
        protected override void Seed(AssetsContext context)
        {
            for (int i = 0; i < 100; i++)
            {
                var ass = new Asset() { Name = "Asset" + i, ChildAssets = GetChildAssets() };
            }
            base.Seed(context);
        }


    }
}