﻿angular.module('epochApp', ['SignalR'])
    .factory('ChatFactory', [
        '$rootScope', 'Hub', function($rootScope, Hub) {
        	var chat = this;

        var message = function(data) {
            if (!message) message = {};
            var msg = {
            	forName: data.forName,
            	body: data.messageBody,
            	dateSent: data.dataSent,
				fromName: data.fromName
			    
            }
            return msg;
        };

        chat.NewMessages = [];


        var hub = new Hub('chat', {
        	'showMessage' : function(msg) {
        		chat.NewMessages.push(msg);
        		$rootScope.$apply();
	        }
        }, ['SendMessage']);

        chat.sendMessage = function(msg) {
        	hub.SendMessage(msg);
        }

        return chat;
    }
    ]);