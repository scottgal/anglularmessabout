﻿(function () {
    'use strict';

    var controllerId = 'epochController';

    // TODO: replace app with your module name
    angular.module('epochApp').controller(controllerId,
        ['$scope', epochController]);

    function epochController($scope) {
        var vm = this;

        vm.activate = activate;
        vm.title = 'epochController';
        vm.Route2 = 'This is Route 2';
        function activate() { }
    }
})();
