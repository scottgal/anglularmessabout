﻿(function () {
    'use strict';

    var id = 'epochApp';

    // TODO: Inject modules as needed.
    var epochApp = angular.module('epochApp', [
        // Angular modules 
        'ngAnimate',        // animations
        'ngRoute'           // routing

        // Custom modules 

        // 3rd Party Modules
        
    ]);

    // Execute bootstrapping code and any dependencies.
    // TODO: inject services as needed.
    epochApp.run(['$q', '$rootScope',
        function ($q, $rootScope) {

        }]);

    epochApp.config(['$routeProvider', function($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'App/Views/Default.html',
            controller: 'epochController'
        })
        .when('/route1', {
            templateUrl: 'App/Views/Route1.html',
            controller: 'epochController'
        })
        .when('/route2', {
            templateUrl: 'App/Views/Route2.html',
            controller: 'epochController'
        })
        .when('/route2', {
            templateUrl: 'App/Views/Route2.html',
            controller: 'epochController'
        });
        ;

    }]);
})();