﻿(function () {
    'use strict';

    var controllerId = 'assetsFormController';

    // TODO: replace app with your module name
    angular.module('epochApp').controller(controllerId,
        ['$scope', assetsFormController]);

    function assetsFormController($scope) {
        var vm = this;


        //Define assets form
        
        var asset = {
            scanIdentifier : '',
            name: '',
            id: ''


        }

        vm.currentAsset = _.cloneDeep(asset);
        
        vm.activate = activate;
        vm.title = 'assetsFormController';



        function activate() { }
    }
})();
