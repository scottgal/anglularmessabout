﻿(function () {
    'use strict';

    var controllerId = 'signalRController';

    // TODO: replace app with your module name
    angular.module('epochApp').controller(controllerId,
        ['$scope', 'ChatFactory', signalRController]);

    function signalRController($scope, chatFactory) {
        var vm = this;

        $scope.$watch(
            function() {
                 return chatFactory.NewMessages;
            },
            function(data) {
                 vm.messages = data;
            }, true
    );

        vm.activate = activate;
        vm.title = 'controller1';

        vm.newMessageText = '';
        vm.sendMessage = function (messageBody) {
            var message = {
                f: 'Scott',
                n: 'B',
                b: messageBody,
                d: new Date()
        }

            chatFactory.sendMessage(message);
        };

    function activate() { }
    }
})();
