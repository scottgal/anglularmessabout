﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace TestApp.Hubs
{
    public class Message
{
        [JsonProperty("f")]
    public string ForName { get; set; }
        [JsonProperty("b")]
        public string MessageBody { get; set; }
         [JsonProperty("n")]
        public string FromName { get; set; }
         [JsonProperty("d")]
        public DateTime? SentDate { get; set; }

}
    [HubName("Chat")]

    public class ChatHub : Hub
    {


        public void SendMessage(Message message)
        {
            Clients.All.showMessage(message);
        }
    }
}