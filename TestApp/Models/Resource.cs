﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using TestDataService.Models;

namespace TestApp.Models
{
    public class Resource : RootObject
    {

        public string Name
        {
            get; set;
        }

        public ResourceType RType
        {
            get; set;
        }

    }
}