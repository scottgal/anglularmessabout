﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestApp.Models
{
    public class AssetComponent : RootObject
    {
        public string Description
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}