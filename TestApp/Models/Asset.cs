﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestApp.Models
{
    public class Asset : RootObject
    {

        public string ScanIdentifier
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
        public List<AssetComponent> Components
        {
            get; set;
        }

        public List<Asset> ChildAssets
        {
            get; set;
        }
    }
}