﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestApp.Helpers;

namespace TestApp.Models
{
    public abstract class RootObject
    {
        protected RootObject()
        {
            Id = SequentialGuid.NewGuid();
        }

        public Guid Id
        {
            get; private set; }
    }
}